module FIFO (in,
wclk,
reset,
wen,
rdclk,
rden ,
out);
input [31:0] in;
input wclk,
reset,
wen,
rdclk,
rden;
output[31:0] out;
//wire [5:0]out1;
//wire [63:0]out2;
//wire [63:0]out3;
//wire [5:0]out4;
wire [5:0] x1,x3;
wire [63:0] x2,x4;
wrpointer wp1(.out1 (x1),
.wclk (wclk),
.reset (reset),
.wen (wen));
wrdec6to64 wrd1 (.in (x1),
.out2 (x2));
core c1 (.in (in),
.wclk (wclk),
.s0 (x2),
.awr (x4),
.out (out));
rddec6to64 rd1(.out3 (x4),
.in (x3));
rdpointer rp1(.out4 (x3),
.rdclk (rdclk),
.rden (rden),
.reset (reset));
endmodule




