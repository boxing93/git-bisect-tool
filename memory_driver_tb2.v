module memory_driver_tb();
	reg button,reset,clk;
	wire valid_in;
	wire [7:0] data_from_counter;
	wire [31:0] data_from_shift,data_from_driver;
	wire [4:0] addr;
	wire empty,mem_full;
	wire [31:0] dataout_from_memory;
	integer j;
	
	memory_driver dut(
		.button(button),
		.reset(reset),
		.clk(clk),
		.valid_in(valid_in),
		.data_from_counter(data_from_counter),
		.data_from_shift(data_from_shift),
		.data_from_driver(data_from_driver),
		.addr(addr),
		.empty(empty),
		.mem_full(mem_full),
		.dataout_from_memory(dataout_from_memory)
			);
			
initial begin
	clk = 1;
end

	always 
		#10 clk = !clk;
		
initial begin
	#10
		reset = 0;
	
	#20
		reset = 1;

		
	for(j = 0; j <= 300; j = j + 1)
		begin
			#50
				button = j;
		end

		#50 $finish;
		
end

endmodule